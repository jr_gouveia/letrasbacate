package br.com.globosat.letrastoptvz.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.R;

public class Util {

	private static String TAG = "TOPTVZ";
	
	public static void log(String mensagem) {
		Log.d(TAG, mensagem);
	}

	public static void log(String mensagem, Throwable erro) {
		if (erro != null) {
			Log.e(TAG, mensagem, erro);
		} else {
			Log.e(TAG, mensagem);
		}
	}

	public static String buscaConteudoWeb(String url)
			throws ClientProtocolException, IOException {
		
		StringBuffer strJson = new StringBuffer(1024);
		HttpGet request = new HttpGet(url);
		request.addHeader("Accept-Encoding", "gzip");
		HttpResponse resp = new DefaultHttpClient().execute(request);
		HttpEntity entity = resp.getEntity();
		Header contentEncoding = resp.getFirstHeader("Content-Encoding");
		InputStream compressed = entity.getContent();
		if (contentEncoding != null
				&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
			compressed = new GZIPInputStream(compressed);
			Util.log("Recebeu json comprimido");
		} else {
			Util.log("Recebeu json sem compressão");
		}
		String str;

		BufferedReader in;
		in = new BufferedReader(new InputStreamReader(compressed));
		while ((str = in.readLine()) != null) {
			strJson.append(str.trim());
		}
		in.close();
		Util.log(strJson.toString());

		return strJson.toString();
	}

	public static void toastErroConexao(Activity atividade) {
		try {
			Toast.makeText(atividade, R.string.msg_precisa_internet,
					Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
		}

	}

	
}
