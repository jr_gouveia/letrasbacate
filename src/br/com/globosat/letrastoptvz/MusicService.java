package br.com.globosat.letrastoptvz;

import java.util.Set;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import br.com.globosat.letrastoptvz.db.TOPTVZOpenHelper;
import br.com.globosat.letrastoptvz.sharedprefs.AppPrefs;
import br.com.globosat.letrastoptvz.task.BuscaMusicaThread;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MusicService extends Service {

	NotificationManager mNM;
	String musicaAtual = "";
	String artistaAtual = "";
	String letraOriginal = "";
	String letraTraducao = "";
	AppPrefs prefs = new AppPrefs();
	boolean playing = false;
	boolean temLetra = false;
	BuscaMusicaThread processo = null;
	LetraTOPTVZ letraEncontrada;
	GoogleAnalyticsTracker tracker;

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// este handler vai receber as mensagens da thread que busca a letra
			switch (msg.what) {
			case 0:
				// neste caso a thread não começou
				break;
			case 1:
				// Log.d("Music", "ta rodando a thread");
				// neste estado a thread está em execução
				break;
			case 2:
				// neste estado a thread acabou sua execução e com isso iremos
				// buscar o objeto encontrado (ou não)
				if (processo != null) {

					letraEncontrada = processo.letraMusica;
					/*
					 * letraOriginal = processo.letraOriginal; letraTraducao =
					 * processo.letraTraduzida;
					 */
					temLetra = processo.temLetra;
					prefs.putBoolean(getApplicationContext(),
							AppPrefs.TEM_LETRA, temLetra);
					if (prefs.getBoolean(getApplicationContext(),
							AppPrefs.TEM_LETRA)) {
						mostraLetra(letraEncontrada);
						salvaMusicaDB(letraEncontrada);
					} else {
						clearNotification();
					}
				}
				break;
			default:
				// qualquer outro estado
				break;
			}

		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		// Pega o serviço de notificação
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", this);

	}

	@Override
	public void onStart(Intent intent, int startId) {
		if (intent != null) {
			Bundle extras = intent.getExtras();
			// verifica se a intent recebida contem dados
			if (!extras.isEmpty()) {
				// Log.d("Music", intent.getStringExtra("actionOriginal"));
				// Log.d("Music", intent.getAction());

				Set<String> keys = extras.keySet();
				for (String key : keys) {
					Log.d("MusicTest",
							"KEY: " + key + "  -  VALOR: " + extras.get(key));
				}

				// testa se o player que está tocando é o meridian
				if (intent.getStringExtra("actionOriginal").startsWith(
						"org.iii.romulus.meridian")) {
					// no meridian a propriedade que indica o play é "status"
					int status = intent.getIntExtra("status", 0);
					if (status == 1) {
						playing = true;
					} else {
						playing = false;
					}

					// indica que não será necessário um controle manual de play
					// e
					// pause
					prefs.putBoolean(getApplicationContext(),
							AppPrefs.CONTROLE_MANUAL, false);
				} else if (intent.getStringExtra("actionOriginal").startsWith(
						"com.adam.aslfms.notify")) {
					// no scrobbling a propriedade que indica o play é "state"
					int status = intent.getIntExtra("state", 3);
					if (status == 0 || status == 1) {
						playing = true;
					} else {
						playing = false;
					}

					// indica que não será necessário um controle manual de play
					// e
					// pause
					prefs.putBoolean(getApplicationContext(),
							AppPrefs.CONTROLE_MANUAL, false);

				} else if (intent.getStringExtra("actionOriginal").startsWith(
						"com.android.music.metachanged")) {
					if (intent.hasExtra("playing")) {
						playing = intent.getBooleanExtra("playing", false);

					} else {
						playing = true;
					}
					prefs.putBoolean(getApplicationContext(),
							AppPrefs.CONTROLE_MANUAL, false);
				} else {
					// verifica se tem o atributo playing que indica estado da
					// musica

					if (intent.hasExtra("playing")
							|| intent.hasExtra("isplaying")
							|| intent.hasExtra("playstate")) {
						// Log.d("Music", "TEM O EXTRA PLAYING");
						if (intent.hasExtra("playing")) {
							playing = intent.getBooleanExtra("playing", false);
							prefs.putBoolean(getApplicationContext(),
									AppPrefs.CONTROLE_MANUAL, false);
						} else if (intent.hasExtra("isplaying")) {
							// REALPLAYER
							playing = intent
									.getBooleanExtra("isplaying", false);
							prefs.putBoolean(getApplicationContext(),
									AppPrefs.CONTROLE_MANUAL, false);
						} else if (intent.hasExtra("playstate")) {
							// REALPLAYER
							playing = intent
									.getBooleanExtra("playstate", false);
							prefs.putBoolean(getApplicationContext(),
									AppPrefs.CONTROLE_MANUAL, false);
						} else {
							prefs.putBoolean(getApplicationContext(),
									AppPrefs.CONTROLE_MANUAL, true);
							playing = controleManual(intent
									.getStringExtra("actionOriginal"));
						}
					} else {
						Log.d("Music", "NAO TEM O EXTRA PLAYING");

						// se não tiver o atributo, seta que o controle será
						// manual
						// (geralmente em android 1.5 e 1.6
						prefs.putBoolean(getApplicationContext(),
								AppPrefs.CONTROLE_MANUAL, true);
						playing = controleManual(intent
								.getStringExtra("actionOriginal"));
					}
				}
				if (playing) {

					// se estiver tocando a musica, verifica se a música mudou
					// para
					// não fazer consulta a toa
					String artist = intent.getStringExtra("artist");
					String album = intent.getStringExtra("album");
					String track = intent.getStringExtra("track");
					Log.d("Music", artist + " : " + album + " : " + track);
					if (!(musicaAtual.equalsIgnoreCase(track) && artistaAtual
							.equalsIgnoreCase(artist))) {
						// zera a notificação
						clearNotification();
						// tenta recuperar a letra no banco de dados
						LetraTOPTVZ letra = consultaMusicaDB(track, artist);
						if (letra != null) {
							artistaAtual = artist;
							musicaAtual = track;
							prefs.putString(getApplicationContext(),
									AppPrefs.ARTISTA, artistaAtual);
							prefs.putString(getApplicationContext(),
									AppPrefs.MUSICA, musicaAtual);
							prefs.putBoolean(getApplicationContext(),
									AppPrefs.TEM_LETRA, true);
							mostraLetra(letra);
							tracker.trackPageView("/buscou-letra-no-banco");
						} else {
							// não encontrou a letra no banco, então tenta
							// encontrar
							// no serviço do toptvz
							consultaMusica(track, artist);
							artistaAtual = artist;
							musicaAtual = track;
							prefs.putString(getApplicationContext(),
									AppPrefs.ARTISTA, artistaAtual);
							prefs.putString(getApplicationContext(),
									AppPrefs.MUSICA, musicaAtual);
							tracker.trackPageView("/buscou-letra-na-web");

						}
						tracker.dispatch();

					}
				} else {
					clearNotification();
				}

			} else {
				clearNotification();
			}
		}
	}

	private boolean controleManual(String action) {
		if (action.contains("playstatechanged")) {
			// se tiver trocando o estado do play, inverte a propriedade manual,
			// ou se esta não existir seta true
			Log.d("Music", "playstatechanged ");
			if (prefs.contains(getApplicationContext(), AppPrefs.STATUS)) {
				boolean status = prefs.getBoolean(getApplicationContext(),
						AppPrefs.STATUS);
				prefs.putBoolean(getApplicationContext(), AppPrefs.STATUS,
						!status);
				// Log.d("Music",
				// "RECUPEROU STATUS ANTIGO "+status+"  STATUS NOVO "+!status);
				return !status;
			} else {
				prefs.putBoolean(getApplicationContext(), AppPrefs.STATUS, true);
				// Log.d("Music", "Criou um novo status ");
				return true;
			}
		} else if (action.contains("metachanged")) {
			// se for uma mudança de música seta true sempre para que o serviço
			// busque a nova letra
			prefs.putBoolean(getApplicationContext(), AppPrefs.STATUS, true);
			// Log.d("Music", "Criou um novo status pela mudança de música ");
			return true;
		}
		// Log.d("Music",
		// " nao era chage RETORNOU "+prefs.getBoolean(getApplicationContext(),
		// AppPrefs.STATUS));
		return prefs.getBoolean(getApplicationContext(), AppPrefs.STATUS);
	}

	private void showNotification(String artista, String musica) {
		// Seta um ID único para a notificação e um texto que aparecerá na
		// notificação
		Resources res = getResources();
		mNM.cancel(111222999);
		CharSequence text = res.getString(R.string.txt_ticker_notificacao);

		// seta o icone e o horário da notificação
		Notification notification = new Notification(R.drawable.icontoptvz,
				text, System.currentTimeMillis());

		// cria a pending intent que será executada quando o usuário apertar
		// sobre a notificação da letra
		Intent intent = new Intent();
		intent.setAction("br.com.android.toptvz.letraencontrada");
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				intent, 0);

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this,
				String.format(res.getString(R.string.txt_titulo_notificacao),
						musica, artista), text, contentIntent);

		// indica que a notificação será apenas visual e que o usuário não
		// poderá limpar.
		notification.defaults = Notification.DEFAULT_LIGHTS;
		// notification.flags = Notification.FLAG_NO_CLEAR;

		// envia a notificação para o usuário
		mNM.notify(111222999, notification);

	}

	private void clearNotification() {
		// cancela a notificação e limpa as variáveis guardadas no shared prefs
		// da aplicação
		mNM.cancel(111222999);
		musicaAtual = "";
		artistaAtual = "";
		prefs.remove(getApplicationContext(), AppPrefs.ARTISTA);
		prefs.remove(getApplicationContext(), AppPrefs.MUSICA);
		prefs.remove(getApplicationContext(), AppPrefs.LETRA);
		prefs.remove(getApplicationContext(), AppPrefs.LETRA_TRADUZIDA);
		prefs.putBoolean(getApplicationContext(), AppPrefs.TEM_LETRA, false);

		// se existir um processo ativo, cancela este
		if (processo != null) {
			// Log.d("Music",
			// "Cancelando o processo : "+processo.artista+" - "+processo.musica);
			processo.interrupt();
			processo = null;
		}
		Intent intentBroadcast = new Intent();
		intentBroadcast.setAction("br.com.android.toptvz.letraperdida");
		sendBroadcast(intentBroadcast);
	}

	private void consultaMusica(String musica, String artista) {
		// URL url;
		// URL urlLetra;
		try {

			// verifica se o telefone tem algum tipo de conexão de internet
			ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivityManager.getActiveNetworkInfo() != null
					&& connectivityManager.getActiveNetworkInfo().isConnected()) {

				// cria e inicia a thread que fará a consulta da letra nos
				// servidores do toptvz
				processo = new BuscaMusicaThread(handler, "", artista, musica);
				processo.start();

				// url = new
				// URL("http://toptvz.com.br/servicos/buscamusica?artista="+URLEncoder.encode(artista.trim(),"UTF-8")+"&musica="+URLEncoder.encode(musica.trim(),"UTF-8"));
				//
				// Log.i("Music", url.toString());
				// BufferedReader in;
				// in = new BufferedReader(new
				// InputStreamReader(url.openStream()));
				// String str;
				// String strJson = "";
				// while ((str = in.readLine()) != null) {
				// strJson += str;
				// }
				// in.close();
				// JSONObject json;
				// json = new JSONObject(strJson);
				// int total = json.getInt("total");
				// Log.i("Music", "RESPOSTA TOTAL: "+total);
				// if(total > 0){
				// prefs.putBoolean(getApplicationContext(), AppPrefs.TEM_LETRA,
				// true);
				// JSONObject musicaJson = json.getJSONObject("resultado");
				// int idMusica = musicaJson.getInt("idMusica");
				//
				// urlLetra = new
				// URL("http://toptvz.com.br/servicos/letras/"+idMusica);
				// Log.i("Music", urlLetra.toString());
				//
				// /* Get a SAXParser from the SAXPArserFactory. */
				// SAXParserFactory spf = SAXParserFactory.newInstance();
				// SAXParser sp = spf.newSAXParser();
				//
				// /* Get the XMLReader of the SAXParser we created. */
				// XMLReader xr = sp.getXMLReader();
				// /* Create a new ContentHandler and apply it to the
				// XML-Reader*/
				// LetraHandler myLetraHandler = new LetraHandler();
				// xr.setContentHandler(myLetraHandler);
				//
				// /* Parse the xml-data from our URL. */
				// xr.parse(new InputSource(new
				// InputStreamReader(urlLetra.openStream())));
				// /* Parsing has finished. */
				//
				// /* Our ExampleHandler now provides the parsed data to us. */
				// LetraParsed parsedLetra = myLetraHandler.getParsedData();
				//
				// /* Set the result to be displayed in our GUI. */
				// String letraOriginal = parsedLetra.getOriginal();
				// Log.d("Music", "letra original: "+letraOriginal);
				// prefs.putString(getApplicationContext(), AppPrefs.LETRA,
				// letraOriginal);
				// String letraTraduzida = parsedLetra.getTraducao();
				// Log.d("Music", "letra traduzida: "+letraTraduzida);
				// prefs.putString(getApplicationContext(),
				// AppPrefs.LETRA_TRADUZIDA, letraTraduzida);
				//
				// }else{
				// prefs.putBoolean(getApplicationContext(), AppPrefs.TEM_LETRA,
				// false);
				// prefs.remove(getApplicationContext(), AppPrefs.LETRA);
				// prefs.remove(getApplicationContext(),
				// AppPrefs.LETRA_TRADUZIDA);
				// }
			}
		} catch (Exception e) {
			Log.e("ERRO", e.getMessage(), e);

		}

	}

	private LetraTOPTVZ consultaMusicaDB(String musica, String artista) {
		// pega uma instancia do banco
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();
		if (artista != null && musica != null) {
			// executa a query pra saber se a letra existe
			String query = "select * from "
					+ TOPTVZOpenHelper.TOPTVZ_TABLE_NAME + " where artista = "
					+ DatabaseUtils.sqlEscapeString(artista) + " and musica = "
					+ DatabaseUtils.sqlEscapeString(musica) + " limit 1";
			// Log.d("Music", query);
			Cursor cursor = db.rawQuery(query, null);
			// verifica se o banco retornou alguma coisa
			if (cursor.getCount() > 0) {
				// posiciona no primeiro registro retornado e pega os valores
				cursor.moveToFirst();
				int idMusicaDB = cursor.getInt(cursor
						.getColumnIndex("_id_musica"));
				int idArtistaDB = cursor.getInt(cursor
						.getColumnIndex("id_artista"));
				String artistaDB = cursor.getString(cursor
						.getColumnIndex("artista"));
				String musicaDB = cursor.getString(cursor
						.getColumnIndex("musica"));
				String letraDB = cursor.getString(cursor
						.getColumnIndex("letra"));
				String letraTraduzidaDB = cursor.getString(cursor
						.getColumnIndex("letra_traduzida"));

				LetraTOPTVZ letraEncontrada = new LetraTOPTVZ(idMusicaDB,
						idArtistaDB, artistaDB, musicaDB, letraDB,
						letraTraduzidaDB, "", "");

				// fecha o cursor e o banco e retorna o objeto encontrado
				cursor.close();
				if (db != null && db.isOpen())
					db.close();

				Log.d("Music", "letra encontrada no banco de dados");
				return letraEncontrada;

			}
			cursor.close();
			if (db != null && db.isOpen())
				db.close();
		}
		return null;

	}

	private boolean salvaMusicaDB(LetraTOPTVZ letra) {
		// pega a instancia do banco
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();

		// seta os valores a serem gravados
		ContentValues values = new ContentValues();
		values.put("_id_musica", letra.getIdMusica());
		values.put("id_artista", letra.getIdArtista());
		values.put("artista", letra.getArtista());
		values.put("musica", letra.getMusica());
		values.put("letra", letra.getLetra());
		values.put("letra_traduzida", letra.getLetraTraduzida());

		// tenta inserir, se o retorno for -1 é pq algum erro aconteceu
		long id = db.insert(TOPTVZOpenHelper.TOPTVZ_TABLE_NAME, null, values);
		if (id >= 0) {
			Log.d("Music", "gravou letra encontrada no banco de dados");
		} else {
			Log.d("Music",
					"nao conseguiu gravar letra encontrada no banco de dados");
		}
		if (db != null && db.isOpen())
			db.close();
		return true;
	}

	private void mostraLetra(LetraTOPTVZ letra) {
		// envia um broadcast para com as letras encontradas
		prefs.putString(getApplicationContext(), AppPrefs.LETRA,
				letra.getLetra());
		prefs.putString(getApplicationContext(), AppPrefs.LETRA_TRADUZIDA,
				letra.getLetraTraduzida());
		showNotification(letra.getArtista(), letra.getMusica());
		Intent intentBroadcast = new Intent();
		intentBroadcast.setAction("br.com.android.toptvz.letraencontrada");
		sendBroadcast(intentBroadcast);
	}
}
