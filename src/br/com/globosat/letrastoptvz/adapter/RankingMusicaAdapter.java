package br.com.globosat.letrastoptvz.adapter;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.globosat.letrastoptvz.R;
import br.com.globosat.letrastoptvz.R.id;
import br.com.globosat.letrastoptvz.R.layout;
import br.com.globosat.letrastoptvz.vo.Artista;
import br.com.globosat.letrastoptvz.vo.Musica;

public class RankingMusicaAdapter extends ArrayAdapter<Musica>{

	private ArrayList<Musica> items;
	private Context context;
	private View v;


	public RankingMusicaAdapter(Context context, int textViewResourceId,
			ArrayList<Musica> items) {
		super(context, textViewResourceId, items);

		this.items = items;
		this.context = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.ranking_musica_item, null);
		}

		Musica t = items.get(position);

		if (t != null) {
			TextView nomeBanda = (TextView) v.findViewById(R.id.textNomeBanda);
			TextView tituloMusica = (TextView) v.findViewById(R.id.textTituloMusica);
			TextView posicao = (TextView) v.findViewById(R.id.textPosicaoRankingMusica);
			String artista = t.getArtista();
			String musica = t.getTitulo();
			String pos = String.valueOf(position+1);
			if (tituloMusica != null) {
				tituloMusica.setText(musica);
			}
			if (nomeBanda != null) {
				nomeBanda.setText(artista);
			}
			if (posicao != null) {
				posicao.setText(pos);
			}
		}

		return v;
	}


	
}
