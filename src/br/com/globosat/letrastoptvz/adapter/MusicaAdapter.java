package br.com.globosat.letrastoptvz.adapter;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.globosat.letrastoptvz.R;
import br.com.globosat.letrastoptvz.R.id;
import br.com.globosat.letrastoptvz.R.layout;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

public class MusicaAdapter extends ArrayAdapter<LetraTOPTVZ>{

	private ArrayList<LetraTOPTVZ> items;
	private Context context;
	private View v;

	static class ViewHolder {
		public TextView textTime;
		public TextView textCartola;
		public StringBuilder textBuilder = new StringBuilder();
	}


	public MusicaAdapter(Context context, int textViewResourceId,
			ArrayList<LetraTOPTVZ> items) {
		super(context, textViewResourceId, items);

		this.items = items;
		this.context = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.musica_item, null);
		}

		LetraTOPTVZ t = items.get(position);

		if (t != null) {
			TextView tituloMusica = (TextView) v.findViewById(R.id.textTituloMusica);
			TextView nomeBanda = (TextView) v.findViewById(R.id.textNomeBanda);
			String artista = t.getArtista();
			String musica = t.getMusica();
			if (tituloMusica != null) {
				tituloMusica.setText(musica);
			}
			if (nomeBanda != null) {
				nomeBanda.setText(artista);
			}
		}

		return v;
	}


	
}
