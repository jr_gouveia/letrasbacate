package br.com.globosat.letrastoptvz.adapter;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.globosat.letrastoptvz.R;
import br.com.globosat.letrastoptvz.R.id;
import br.com.globosat.letrastoptvz.R.layout;
import br.com.globosat.letrastoptvz.vo.Artista;

public class RankingArtistaAdapter extends ArrayAdapter<Artista>{

	private ArrayList<Artista> items;
	private Context context;
	private View v;
	private boolean blnPosicao = true;


	public RankingArtistaAdapter(Context context, int textViewResourceId,
			ArrayList<Artista> items) {
		super(context, textViewResourceId, items);

		this.items = items;
		this.context = context;
	}
	
	public RankingArtistaAdapter(Context context, int textViewResourceId,
			ArrayList<Artista> items, boolean blnPosicao) {
		super(context, textViewResourceId, items);

		this.items = items;
		this.context = context;
		this.blnPosicao = blnPosicao;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.ranking_artista_item, null);
		}

		Artista t = items.get(position);

		if (t != null) {
			TextView nomeBanda = (TextView) v.findViewById(R.id.textTituloArtista);
			TextView posicao = (TextView) v.findViewById(R.id.textPosicaoRankingArtistas);
			String artista = t.getNome();
			String pos = String.valueOf(position+1);
			if (nomeBanda != null) {
				nomeBanda.setText(artista);
			}
			if(blnPosicao){
				if (posicao != null) {
					posicao.setText(pos);
				}
			} else {
				posicao.setVisibility(View.GONE);
			}
		}

		return v;
	}


	
}
