package br.com.globosat.letrastoptvz.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class AppPrefs {

	public final static String PREFS_KEY = "TOPTVZLYRICS";

	public final static String ARTISTA = "artista";
	public final static String MUSICA = "musica";
	public final static String LETRA = "letraOriginal";
	public final static String LETRA_TRADUZIDA = "letraTraduzida";
	public final static String TEM_LETRA = "temLetra";
	public final static String STATUS = "statusPlayer";
	public final static String CONTROLE_MANUAL = "controleManual";
	public final static String TAMANHO_FONTE = "tamanhoFonte";
	public final static String ALINHAMENTO = "alinhamento";
	public final static String ALINHAMENTO_CENTER = "center";
	public final static String ALINHAMENTO_LEFT = "left";
	public final static boolean TEM_LETRA_DEFAULT = false;
	public AppPrefs() {
		
	}
	
	public void putString(Context context, String chave, String valor) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putString(chave, valor);
				edit.commit();
				Log.d("PREFS", "putString("+chave+" : "+valor+") do widget "+PREFS_KEY);
			}
		}
	}

	public void putInt(Context context, String chave, int valor) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putInt(chave, valor);
				edit.commit();
				Log.d("PREFS", "putInt("+chave+" : "+valor+") do widget "+PREFS_KEY);
			}
		}
	}
	
	public void putFloat(Context context, String chave, Float valor) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putFloat(chave, valor);
				edit.commit();
				Log.d("PREFS", "putFloat("+chave+" : "+valor+") do widget "+PREFS_KEY);
			}
		}
	}
	
	public void putBoolean(Context context, String chave, boolean valor) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putBoolean(chave, valor);
				edit.commit();
				Log.d("PREFS", "putBoolean("+chave+" : "+valor+") do widget "+PREFS_KEY);
			}
		}
	}
	
	public String getString(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getString("+chave+" : "+prefs.getString(chave, "INFORMA")+") do widget "+PREFS_KEY);
			return prefs.getString(chave, null);
		}
		return null;
	}
	
	public int getInt(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getInt("+chave+" : "+prefs.getInt(chave, 0)+") do widget "+PREFS_KEY);
			return prefs.getInt(chave, 0);
		}
		return -1;
	}
	
	public Float getFloat(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getFloat("+chave+" : "+prefs.getFloat(chave, 0)+") do widget "+PREFS_KEY);
			return prefs.getFloat(chave, Float.valueOf("16"));
		}
		return Float.valueOf("16");
	}
	
	public boolean getBoolean(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getBoolean("+chave+" : "+prefs.getBoolean(chave, false)+") do widget "+PREFS_KEY);
			return prefs.getBoolean(chave, false);
		}
		return false;
	}
	
	public boolean contains(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			//Log.d("PREFS", "contains("+chave+" : "+prefs.getBoolean(chave, false)+") do widget "+PREFS_KEY);
			return prefs.contains(chave);
		}
		return false;
	}
	
	public boolean remove(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.remove(chave);
				return edit.commit();
			}
		}
		return false;
	}
	
	
	public static void delete(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
		if (prefs != null) {
			prefs.edit().clear();
			prefs.edit().commit();
			
		}
	}

}
