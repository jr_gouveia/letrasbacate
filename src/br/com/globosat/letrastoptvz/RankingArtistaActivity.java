package br.com.globosat.letrastoptvz;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.task.BuscaRankingArtistasTask;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class RankingArtistaActivity extends SherlockFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_ranking);
		
		ActionBar ab = getSupportActionBar();
		ab.setTitle("top artistas");
		ab.setDisplayHomeAsUpEnabled(true);
		
		ConnectivityManager connectivityManager = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getActiveNetworkInfo() != null
				&& connectivityManager.getActiveNetworkInfo().isConnected()) {
			BuscaRankingArtistasTask task = new BuscaRankingArtistasTask(this);
			task.execute(0);
		} else {
			Toast.makeText(getApplicationContext(), R.string.msg_precisa_internet,
					Toast.LENGTH_LONG).show();
			finish();
		}
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			// put your code here
		}
		return false;
	}
}
