package br.com.globosat.letrastoptvz.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;
import br.com.globosat.letrastoptvz.MusicService;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

public class BuscaMusicaThread extends Thread{

	// ***************************
	// Public Constants
	// ***************************
	public final static byte STATE_NOT_STARTED = 0;
	public final static byte STATE_RUNNING = 1;
	public final static byte STATE_DONE = 2;
	public final static byte STATE_ERROR = 3;


	private byte mbytStatus = STATE_NOT_STARTED;
	public Handler HandlerOfCaller;
	public MusicService contexto;
	public String mensagem;
	public boolean temLetra;
	public String letraOriginal = "";
	public String letraTraduzida = "";
	public String artista;
	public String musica;
	public int idArtista;
	public int idMusica;
	public LetraTOPTVZ letraMusica;

	public BuscaMusicaThread(Handler oHandler, String mensagem, String artista, String musica) 
	{
		HandlerOfCaller = oHandler;
		this.mensagem = mensagem;
		this.artista = artista;
		this.musica = musica;
		HandlerOfCaller.sendEmptyMessage(0);
	}

	@Override
	public void run() 
	{
		try{
			HandlerOfCaller.sendEmptyMessage(1);
			mbytStatus = STATE_RUNNING;	

			URL url;
			URL urlLetra;
			try {
				//começa a consulta aos serviços do toptvz
				//url = new URL("http://toptvz.com.br/servicos/buscamusica?artista="+URLEncoder.encode(artista.trim(),"UTF-8")+"&musica="+URLEncoder.encode(musica.trim(),"UTF-8"));
				url = new URL("http://toptvz.com.br/api/api.json.js?metodo=buscamusica&artista="+URLEncoder.encode(artista.trim(),"UTF-8")+"&musica="+URLEncoder.encode(musica.trim(),"UTF-8"));
				
				Log.d("Music", url.toString());
				BufferedReader in;
				in = new BufferedReader(new InputStreamReader(url.openStream()));
				String str;
				String strJson = ""; 
				while ((str = in.readLine()) != null) {
					strJson += str; 
				}
				in.close();
				//recria o json recebido da primeira consulta
				JSONObject json;
				json = new JSONObject(strJson);
				int total = json.getInt("total");
				Log.d("Music", "RESPOSTA TOTAL: "+total);
				if(total > 0){
					//pega os valores do json
					temLetra = true;
					JSONObject musicaJson = json.getJSONObject("resultado");
					idMusica = musicaJson.getInt("idMusica");
					idArtista = musicaJson.getInt("idArtista");
					
					//começa a preparar a chamada para o segundo serviço (o da letra)
//					urlLetra = new URL("http://toptvz.com.br/servicos/letras/"+idMusica);
//					Log.d("Music", urlLetra.toString());
//
//					/* Get a SAXParser from the SAXPArserFactory. */
//					SAXParserFactory spf = SAXParserFactory.newInstance();
//					SAXParser sp = spf.newSAXParser();
//
//					/* Get the XMLReader of the SAXParser we created. */
//					XMLReader xr = sp.getXMLReader();
//					/* Create a new ContentHandler and apply it to the XML-Reader*/
//					LetraHandler myLetraHandler = new LetraHandler();
//					xr.setContentHandler(myLetraHandler);
//
//					/* Parse the xml-data from our URL. */
//					xr.parse(new InputSource(new InputStreamReader(urlLetra.openStream())));
//					/* Parsing has finished. */
//
//					/* Our ExampleHandler now provides the parsed data to us. */
//					LetraParsed parsedLetra = myLetraHandler.getParsedData();

					/* Set the result to be displayed in our GUI. */
//					letraOriginal = parsedLetra.getOriginal();
					if(musicaJson.has("letra")){
						letraOriginal = musicaJson.getString("letra");
						Log.d("Music", "letra original: "+letraOriginal);
					}else{
						letraOriginal = "";
					}
					//prefs.putString(getApplicationContext(), AppPrefs.LETRA, letraOriginal);

//					letraTraduzida = parsedLetra.getTraducao();
					if(musicaJson.has("letraTraduzida") &&(! musicaJson.getString("letraTraduzida").equalsIgnoreCase("null"))){
						letraTraduzida = musicaJson.getString("letraTraduzida");
						Log.d("Music", "letra traduzida: "+letraTraduzida);
					}else{
						letraTraduzida = "";
					}
					
					//prefs.putString(getApplicationContext(), AppPrefs.LETRA_TRADUZIDA, letraTraduzida);

					//cria o objeto resultado das consultas
					letraMusica = new LetraTOPTVZ(idMusica, idArtista, artista, musica, letraOriginal, letraTraduzida);

				}else{
					//prefs.putBoolean(getApplicationContext(), AppPrefs.TEM_LETRA, false);
					temLetra = false;
					//prefs.remove(getApplicationContext(), AppPrefs.LETRA);
					letraOriginal = "";
					//prefs.remove(getApplicationContext(), AppPrefs.LETRA_TRADUZIDA);
					letraTraduzida = "";
				}
				//HandlerOfCaller.sendEmptyMessage(2);

			}catch (Exception e) {
				Log.e("ERRO", e.getMessage(), e);
				HandlerOfCaller.sendEmptyMessage(3);
			}


		}catch (Exception ex) 
		{   
			// Return the error via the handler
			/*Message oMessage = HandlerOfCaller.obtainMessage();
			Bundle oBundle = new Bundle();
			String strMessage = ex.getMessage();
			oBundle.putString("Message", strMessage);
			oMessage.setData(oBundle);
			oMessage.what = MESSAGE_ERROR;
			HandlerOfCaller.sendMessage(oMessage);*/

			//envia um aviso de erro
			ex.printStackTrace();
			HandlerOfCaller.sendEmptyMessage(3);
		}
		
		//envia um aviso de que acabou o processamento
		mbytStatus = STATE_DONE;
		HandlerOfCaller.sendEmptyMessage(2);
	}

	public byte GetStatus()
	{
		return mbytStatus;
	}

}
