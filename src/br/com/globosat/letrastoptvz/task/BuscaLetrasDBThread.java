package br.com.globosat.letrastoptvz.task;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import br.com.globosat.letrastoptvz.db.TOPTVZOpenHelper;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

public class BuscaLetrasDBThread extends Thread {

	// ***************************
	// Public Constants
	// ***************************
	public final static byte STATE_NOT_STARTED = 0;
	public final static byte STATE_RUNNING = 1;
	public final static byte STATE_DONE = 2;
	public final static byte STATE_ERROR = 3;

	private byte mbytStatus = STATE_NOT_STARTED;
	public Handler HandlerOfCaller;
	private Context contexto;
	public ArrayList<LetraTOPTVZ> lista = null;

	// ***************************
	// Constructors
	// ***************************
	// Thread constructor
	public BuscaLetrasDBThread(Handler oHandler, Context contexto) {
		HandlerOfCaller = oHandler;
		this.contexto = contexto;

	}

	// **********************************
	// Public methods
	// **********************************
	// This method is run when the thread
	// is started by calling start().
	@Override
	public void run() {
		try {
			mbytStatus = STATE_RUNNING;
			consultaMusicasDB();
		} catch (Exception ex) {
			// Return the error via the handler
			/*
			 * Message oMessage = HandlerOfCaller.obtainMessage(); Bundle
			 * oBundle = new Bundle(); String strMessage = ex.getMessage();
			 * oBundle.putString("Message", strMessage);
			 * oMessage.setData(oBundle); oMessage.what = MESSAGE_ERROR;
			 * HandlerOfCaller.sendMessage(oMessage);
			 */
			ex.printStackTrace();
		}
		mbytStatus = STATE_DONE;
	}

	// Get the running status of the threaded process
	public byte GetStatus() {
		return mbytStatus;
	}


	private void consultaMusicasDB() {

		try {
			// pega uma instancia do banco
			SQLiteDatabase db = null;
			TOPTVZOpenHelper toh = new TOPTVZOpenHelper(contexto);
			db = toh.getWritableDatabase();
			ArrayList<LetraTOPTVZ> listaLetras = new ArrayList<LetraTOPTVZ>();
			// executa a query pra saber se a letra existe
			String query = "select * from "
					+ TOPTVZOpenHelper.TOPTVZ_TABLE_NAME +" order by musica";
			// + " order by musica asc";
			// Log.d("Music", query);
			Cursor cursor = db.rawQuery(query, null);
			// verifica se o banco retornou alguma coisa
			if (cursor.getCount() > 0) {
				// posiciona no primeiro registro retornado e pega os valores
				cursor.moveToFirst();
				do {
					int idMusicaDB = cursor.getInt(cursor
							.getColumnIndex("_id_musica"));
					int idArtistaDB = cursor.getInt(cursor
							.getColumnIndex("id_artista"));
					String artistaDB = cursor.getString(cursor
							.getColumnIndex("artista"));
					String musicaDB = cursor.getString(cursor
							.getColumnIndex("musica"));
					String letraDB = cursor.getString(cursor
							.getColumnIndex("letra"));
					String letraTraduzidaDB = cursor.getString(cursor
							.getColumnIndex("letra_traduzida"));

					LetraTOPTVZ letraEncontrada = new LetraTOPTVZ(idMusicaDB,
							idArtistaDB, artistaDB, musicaDB, letraDB,
							letraTraduzidaDB, "", "");

					listaLetras.add(letraEncontrada);

					cursor.moveToNext();
				} while (!cursor.isAfterLast());

			}
			cursor.close();
			if (db != null && db.isOpen())
				db.close();
			mbytStatus = STATE_DONE;
			lista = listaLetras;
		} catch (Exception e) {
			mbytStatus = STATE_ERROR;
		}
		HandlerOfCaller.sendEmptyMessage(0);
	}

}