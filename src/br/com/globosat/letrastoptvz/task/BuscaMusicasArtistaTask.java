package br.com.globosat.letrastoptvz.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.provider.MediaStore.Video;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.R;
import br.com.globosat.letrastoptvz.adapter.MusicasArtistaAdapter;
import br.com.globosat.letrastoptvz.vo.Artista;
import br.com.globosat.letrastoptvz.vo.Musica;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class BuscaMusicasArtistaTask extends
		AsyncTask<Artista, Void, ArrayList<Musica>> {
	Activity atividade;
	ArrayList<Musica> listMusicas = new ArrayList<Musica>();
	GoogleAnalyticsTracker tracker;

	public BuscaMusicasArtistaTask(Activity atividade) {
		super();
		this.atividade = atividade;
	}

	@Override
	protected ArrayList<Musica> doInBackground(Artista... params) {
		Artista artista = params[0];
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", atividade);
		tracker.trackPageView("/musicas/" + artista.getNomePagina());

		try {
			String strJson = "";
			URL url;
			url = new URL(
					"http://toptvz.com.br/api/api.json.js?metodo=musicasPorArtista&artista="
							+ artista.getNomePagina() + "&sem_letra=1");
			// Log.d("Music", "chamada em: "+url);
			BufferedReader in;
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;

			while ((str = in.readLine()) != null) {
				strJson += str;
			}
			in.close();
			JSONArray jMusicas = new JSONArray(strJson);

			for (int i = 0; i < jMusicas.length(); i++) {
				JSONObject jMusica = jMusicas.getJSONObject(i);
				String titulo = jMusica.getString("nome");
				// String tituloPagina = jMusica.getString("tituloPagina");

				Musica musica = new Musica(titulo, "", artista.getNome(),
						artista.getNomePagina());
				listMusicas.add(musica);
			}
			return listMusicas;
		} catch (Exception e) {
			Log.e("Music", "erro na chamada do json", e);
		}
		return null;
	}

	OnItemClickListener listenerMusica = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			Musica m = listMusicas.get(position);
			Intent intent = new Intent("br.com.android.toptvz.repassamusica");
			intent.putExtra("musicaTOP", m);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			atividade.startActivity(intent);
		}

	};

	@Override
	protected void onPostExecute(ArrayList<Musica> listaDeMusicas) {

		ProgressBar loading = (ProgressBar) atividade
				.findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		if (listaDeMusicas != null) {
			final ListView lv1 = (ListView) atividade
					.findViewById(R.id.ListRanking);
			lv1.setAdapter(new MusicasArtistaAdapter(atividade,
					R.layout.musica_item, listaDeMusicas));
			lv1.setOnItemClickListener(listenerMusica);
		}
		tracker.dispatch();
		super.onPostExecute(listaDeMusicas);
	}

}
