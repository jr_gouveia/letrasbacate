package br.com.globosat.letrastoptvz.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.R;
import br.com.globosat.letrastoptvz.adapter.RankingArtistaAdapter;
import br.com.globosat.letrastoptvz.vo.Artista;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class BuscaRankingArtistasTask extends
		AsyncTask<Integer, Void, ArrayList<Artista>> {
	Activity atividade;
	ArrayList<Artista> listArtistas = new ArrayList<Artista>();
	GoogleAnalyticsTracker tracker;

	public BuscaRankingArtistasTask(Activity atividade) {
		super();
		this.atividade = atividade;
	}

	@Override
	protected ArrayList<Artista> doInBackground(Integer... params) {

		try {
			String strJson = "";
			URL url;
			url = new URL(
					"http://toptvz.com.br/api/api.json.js?metodo=topArtistasAcessados&limite=50");
			BufferedReader in;
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;

			while ((str = in.readLine()) != null) {
				strJson += str;
			}
			in.close();
			JSONArray jArtistas = new JSONArray(strJson);

			for (int i = 0; i < jArtistas.length(); i++) {
				JSONObject jArtista = jArtistas.getJSONObject(i);
				String nomeArtista = jArtista.getString("nome");
				String nomePagina = jArtista.getString("nomePagina");

				Artista artista = new Artista(nomeArtista, nomePagina);
				listArtistas.add(artista);
			}
			return listArtistas;
		} catch (Exception e) {
			Log.e("Music", "erro na chamada do json", e);
		}

		return null;
	}

	OnItemClickListener listenerArtista = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			Intent intent = new Intent("br.com.android.toptvz.musicasdoartista");
			Artista artista = listArtistas.get(position);
			intent.putExtra("artista", artista);
			atividade.startActivity(intent);
		}

	};

	@Override
	protected void onPostExecute(ArrayList<Artista> listaDeArtista) {
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", atividade);
		tracker.trackPageView("/topArtistas");
		ProgressBar loading = (ProgressBar) atividade
				.findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		if (listaDeArtista != null) {
			final ListView lv1 = (ListView) atividade
					.findViewById(R.id.ListRanking);
			lv1.setAdapter(new RankingArtistaAdapter(atividade,
					R.layout.ranking_artista_item, listaDeArtista));
			lv1.setOnItemClickListener(listenerArtista);
		}
		tracker.dispatch();
		super.onPostExecute(listaDeArtista);
	}

}
