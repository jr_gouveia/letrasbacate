package br.com.globosat.letrastoptvz.task;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import br.com.globosat.letrastoptvz.util.Util;
import br.com.globosat.letrastoptvz.vo.Artista;

public class BuscaArtistasTask extends
		AsyncTask<Void, Void, ArrayList<Artista>> {

	Activity atividade;
	View view;
	String urlApi = "";
	URL urlNoticias;
	StringBuffer strJson = new StringBuffer(1024);
	ArrayList<Artista> listaArtistas = new ArrayList<Artista>();
	boolean erroConexao = false;
	String strBusca = "";
	public final static String INTENT = "br.com.android.toptvz.buscaartistas";

	public BuscaArtistasTask(Activity atividade, String strBusca) {
		super();
		this.atividade = atividade;
		this.strBusca = strBusca;
	}

	@Override
	protected ArrayList<Artista> doInBackground(Void... v) {

		return buscaNaWeb();
	}

	@Override
	protected void onPostExecute(ArrayList<Artista> listArtistas) {
		Intent intent = new Intent(INTENT);

		if (listArtistas != null) {
			intent.putExtra("listaArtistas", listArtistas);
		}
		intent.putExtra("erroConexao", erroConexao);
		atividade.sendBroadcast(intent);
		super.onPostExecute(listArtistas);
	}

	protected ArrayList<Artista> buscaNaWeb() {
		ConnectivityManager connectivityManager = (ConnectivityManager) atividade
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getActiveNetworkInfo() != null
				&& connectivityManager.getActiveNetworkInfo().isConnected()) {

			try {
				String str = "";
				str = Util
						.buscaConteudoWeb("http://toptvz.com.br/api/api.json.js?metodo=buscaPorArtista&artista="
								+ Uri.encode(strBusca.trim()));

				JSONArray jArtistas = new JSONArray(str);
				for (int i = 0; i < jArtistas.length(); i++) {
					JSONObject jArtista = jArtistas.getJSONObject(i);
					String nomeArtista = jArtista.getString("artista");
					String nomePagina = jArtista.getString("nomePagina");

					Artista artista = new Artista(nomeArtista, nomePagina);
					listaArtistas.add(artista);
				}
				return listaArtistas;
			} catch (MalformedURLException e) {
				Util.log("ERRO NA URL", e);
			} catch (IOException e) {
				Util.log("ERRO DE IO", e);
			} catch (JSONException e) {
				Util.log("ERRO NA FORMAÇÃO DO JSON", e);
			} catch (Exception e) {
				Util.log("ERRO NAO IDENTIFICADO", e);
			}
		} else {
			erroConexao = true;
		}
		return null;
	}

}
