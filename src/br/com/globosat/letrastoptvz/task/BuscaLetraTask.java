package br.com.globosat.letrastoptvz.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.R;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;
import br.com.globosat.letrastoptvz.vo.Musica;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class BuscaLetraTask extends AsyncTask<Musica, Void, LetraTOPTVZ> {
	Activity atividade;
	ArrayList<LetraTOPTVZ> letra = new ArrayList<LetraTOPTVZ>();
	GoogleAnalyticsTracker tracker;
	public String letraOriginal = "";
	public String letraTraduzida = "";
	public String artista;
	public String musica;
	public int idArtista;
	public int idMusica;
	public LetraTOPTVZ letraMusica;

	public BuscaLetraTask(Activity atividade) {
		super();
		this.atividade = atividade;
	}

	@Override
	protected void onPreExecute() {
		ListView lv1 = (ListView) atividade.findViewById(R.id.ListRanking);
		lv1.setVisibility(View.GONE);
		ProgressBar loading = (ProgressBar) atividade
				.findViewById(R.id.loading);
		loading.setVisibility(View.VISIBLE);
		super.onPreExecute();
	}

	@Override
	protected LetraTOPTVZ doInBackground(Musica... params) {

		Musica musica = params[0];
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", atividade);
		tracker.trackPageView("/letra/" + musica.getArtistaPagina() + "/"
				+ musica.getTituloPagina());

		try {
			String strJson = "";
			URL url;
			String tituloMusicaBusca = "";
			String nomeArtistaBusca = "";
			// if (musica.getTituloPagina() != null
			// && musica.getTituloPagina().trim() != "") {
			// tituloMusicaBusca = musica.getTituloPagina();
			// } else {
			tituloMusicaBusca = URLEncoder.encode(musica.getTitulo().trim(),
					"UTF-8");
			
			nomeArtistaBusca = URLEncoder.encode(musica.getArtista().trim(),
					"UTF-8");
			// }

			url = new URL(
					"http://toptvz.com.br/api/api.json.js?metodo=buscamusica&artista="
							+ nomeArtistaBusca + "&musica="
							+ tituloMusicaBusca);
			Log.d("Music", "chamada em: " + url);
			BufferedReader in;
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;

			while ((str = in.readLine()) != null) {
				strJson += str;
			}
			in.close();
			JSONObject json;
			json = new JSONObject(strJson);
			int total = json.getInt("total");
			Log.d("Music", "RESPOSTA TOTAL: " + total);
			if (total > 0) {
				JSONObject musicaJson = json.getJSONObject("resultado");
				idMusica = musicaJson.getInt("idMusica");
				idArtista = musicaJson.getInt("idArtista");
				if (musicaJson.has("letra")) {
					letraOriginal = musicaJson.getString("letra");
					Log.d("Music", "letra original: " + letraOriginal);
				} else {
					letraOriginal = "";
				}
				// prefs.putString(getApplicationContext(), AppPrefs.LETRA,
				// letraOriginal);

				// letraTraduzida = parsedLetra.getTraducao();
				if (musicaJson.has("letraTraduzida")
						&& (!musicaJson.getString("letraTraduzida")
								.equalsIgnoreCase("null"))) {
					letraTraduzida = musicaJson.getString("letraTraduzida");
					Log.d("Music", "letra traduzida: " + letraTraduzida);
				} else {
					letraTraduzida = "";
				}

				// prefs.putString(getApplicationContext(),
				// AppPrefs.LETRA_TRADUZIDA, letraTraduzida);

				// cria o objeto resultado das consultas
				letraMusica = new LetraTOPTVZ(idMusica, idArtista,
						musica.getArtista(), musica.getTitulo(), letraOriginal,
						letraTraduzida);
				return letraMusica;
			}
		} catch (Exception e) {
			Log.e("Music", "erro na chamada do json", e);
			atividade.finish();
		}
		return null;
	}

	@Override
	protected void onPostExecute(LetraTOPTVZ letra) {

		ProgressBar loading = (ProgressBar) atividade
				.findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		if (letra != null) {
			Intent i = new Intent("br.com.android.toptvz.minhaletrav2");
			i.putExtra("letraTOP", letra);
			atividade.startActivity(i);
			ListView lv1 = (ListView) atividade.findViewById(R.id.ListRanking);
			lv1.setVisibility(View.VISIBLE);
		}
		tracker.dispatch();
		super.onPostExecute(letra);
	}

}
