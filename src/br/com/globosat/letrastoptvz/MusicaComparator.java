package br.com.globosat.letrastoptvz;

import java.util.Comparator;

import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

public class MusicaComparator implements Comparator<LetraTOPTVZ> {
	public int compare(LetraTOPTVZ l1, LetraTOPTVZ l2) {
		String o1 = l1.getMusica().toLowerCase();
		String o2 = l2.getMusica().toLowerCase();
		o1 = converte(o1);
		o2 = converte(o2);
		o1 = o1.replaceAll("^([0-9]).*$", "$1");
		o2 = o2.replaceAll("^([0-9]).*$", "$1");
		try {
			Integer.parseInt(o1);
			return 1;
		} catch (NumberFormatException e) {
			try {
				Integer.parseInt(o2);
				return -1;
			} catch (NumberFormatException e1) {
				return o1.compareTo(o2);
			}
		}
	}

	public String converte(String text) {

		return text.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e")
				.replaceAll("[îìíï]", "i").replaceAll("[õôòóö]", "o")
				.replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A")
				.replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I")
				.replaceAll("[ÕÔÒÓÖ]", "O").replaceAll("[ÛÙÚÜ]", "U")
				.replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n')
				.replace('Ñ', 'N').replaceAll("!", "")
				.replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ")
				.replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ")
				.replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/]", " ");

	}
}