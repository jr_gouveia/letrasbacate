package br.com.globosat.letrastoptvz.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TOPTVZOpenHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "dbMinhasLetras";
	public static final String TOPTVZ_TABLE_NAME = "toptvz_letras";
    private static final String TOPTVZ_TABLE_CREATE =
                "CREATE TABLE " + TOPTVZ_TABLE_NAME + " (" +
              //  "_id INTEGER PRIMARY KEY, " +
                "_id_musica INTEGER PRIMARY KEY, " +
                "id_artista INTEGER, " +                
                "artista TEXT, " +
                "musica TEXT, " +
                "letra TEXT, " +
                "letra_traduzida TEXT, " +
                "url TEXT, " +
                "url_imagem TEXT) ";
	
	public TOPTVZOpenHelper(Context context) {
		 super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TOPTVZ_TABLE_CREATE);
		Log.d("teste Banco", "criou o banco: "+ TOPTVZ_TABLE_CREATE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
