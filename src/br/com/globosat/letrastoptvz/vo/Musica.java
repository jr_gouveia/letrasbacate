package br.com.globosat.letrastoptvz.vo;

import java.io.Serializable;

public class Musica implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6620101520258339520L;
	private String titulo;
	private String tituloPagina;
	private String artista;
	private String artistaPagina;
	
	public Musica(String titulo, String tituloPagina, String artista,
			String artistaPagina) {
		this.titulo = titulo;
		this.tituloPagina = tituloPagina;
		this.artista = artista;
		this.artistaPagina = artistaPagina;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTituloPagina() {
		return tituloPagina;
	}

	public void setTituloPagina(String tituloPagina) {
		this.tituloPagina = tituloPagina;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getArtistaPagina() {
		return artistaPagina;
	}

	public void setArtistaPagina(String artistaPagina) {
		this.artistaPagina = artistaPagina;
	}

	
}
