package br.com.globosat.letrastoptvz.vo;

import java.io.Serializable;

public class Artista implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private String nomePagina;
	
	public Artista(String nome, String nomePagina) {
		this.nome = nome;
		this.nomePagina = nomePagina;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomePagina() {
		return nomePagina;
	}

	public void setNomePagina(String nomePagina) {
		this.nomePagina = nomePagina;
	}
	
	
}
