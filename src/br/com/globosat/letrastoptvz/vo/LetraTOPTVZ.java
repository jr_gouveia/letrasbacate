package br.com.globosat.letrastoptvz.vo;

import java.io.Serializable;

public class LetraTOPTVZ implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idArtista;
	private int idMusica;
    String artista;
    String musica;
    String letra;
    String letraTraduzida;
    String url;
    String url_imagem;
    
    public LetraTOPTVZ(String artista, String musica, String letra,
			String letraTraduzida) {
		this.artista = artista;
		this.musica = musica;
		this.letra = letra;
		this.letraTraduzida = letraTraduzida;
	}
    
    public LetraTOPTVZ(int idMusica,int idArtista, String artista,
			String musica, String letra, String letraTraduzida) {
		this.idArtista = idArtista;
		this.idMusica = idMusica;
		this.artista = artista;
		this.musica = musica;
		this.letra = letra;
		this.letraTraduzida = letraTraduzida;
	}
    
    public LetraTOPTVZ(int idMusica,int idArtista, String artista,
			String musica, String letra, String letraTraduzida, String url,
			String url_imagem) {
		this.idArtista = idArtista;
		this.idMusica = idMusica;
		this.artista = artista;
		this.musica = musica;
		this.letra = letra;
		this.letraTraduzida = letraTraduzida;
		this.url = url;
		this.url_imagem = url_imagem;
	}
    
	public int getIdArtista() {
		return idArtista;
	}
	public void setIdArtista(int idArtista) {
		this.idArtista = idArtista;
	}
	public int getIdMusica() {
		return idMusica;
	}
	public void setIdMusica(int idMusica) {
		this.idMusica = idMusica;
	}
	public String getArtista() {
		return artista;
	}
	public void setArtista(String artista) {
		this.artista = artista;
	}
	public String getMusica() {
		return musica;
	}
	public void setMusica(String musica) {
		this.musica = musica;
	}
	public String getLetra() {
		return letra;
	}
	public void setLetra(String letra) {
		this.letra = letra;
	}
	public String getLetraTraduzida() {
		return letraTraduzida;
	}
	public void setLetraTraduzida(String letraTraduzida) {
		this.letraTraduzida = letraTraduzida;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl_imagem() {
		return url_imagem;
	}
	public void setUrl_imagem(String url_imagem) {
		this.url_imagem = url_imagem;
	}

}
