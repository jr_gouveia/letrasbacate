package br.com.globosat.letrastoptvz;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.globosat.letrastoptvz.db.TOPTVZOpenHelper;
import br.com.globosat.letrastoptvz.sharedprefs.AppPrefs;
import br.com.globosat.letrastoptvz.util.Util;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class InicialActivity extends SherlockFragmentActivity {

	GoogleAnalyticsTracker tracker;
	AppPrefs prefs;
	Context contexto;
	boolean temLetra = false;
	Resources res;
	IntentFilter iF;
	ImageButton btnBuscar;
	EditText campoBusca;
	LinearLayout botaoLetra, botaoMinhasLetras, botaoTopArtista, botaoTopMusicas;
	ImageView icone, iconeMinhasLetras, setinhaMinhasLetras, setinhaLetra;
	TextView labelMusica, labelChapeu, labelArtista, labelMinhasMusicas,
			labelMinhasMusicasTitulo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_home_v2);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", this);
		tracker.trackPageView("/home");
		prefs = new AppPrefs();
		res = getResources();
		
		ActionBar ab = getSupportActionBar();
		ab.hide();

		contexto = getApplicationContext();
		botaoLetra = (LinearLayout) findViewById(R.id.botaoLetra);

		icone = (ImageView) findViewById(R.id.iconeBotaoLetra);
		labelChapeu = (TextView) findViewById(R.id.textoBotaoLetraChapeu);
		labelMusica = (TextView) findViewById(R.id.textoBotaoLetraMusica);
		labelArtista = (TextView) findViewById(R.id.textoBotaoLetraArtista);
		testaLetra();

		

		botaoTopArtista = (LinearLayout) findViewById(R.id.botaoTopArtistas);
		botaoTopArtista.setOnTouchListener(btnTouchRankingListener);
		botaoTopArtista.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent("br.com.android.toptvz.topartistas");
				startActivity(i);

			}
		});
		
		botaoTopMusicas = (LinearLayout) findViewById(R.id.botaoTopMusicas);
		botaoTopMusicas.setOnTouchListener(btnTouchRankingListener);
		botaoTopMusicas.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent("br.com.android.toptvz.topmusicas");
				startActivity(i);

			}
		});
		
		campoBusca = (EditText) findViewById(R.id.editTextBuscaArtista);
		
		
		btnBuscar = (ImageButton) findViewById(R.id.imageButtonBuscar);
		btnBuscar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				buscar();
			}
		});
		
		campoBusca.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
		            buscar();
		            return true;
		        }
		        return false;
		    }
		});
		
		tracker.dispatch();

		iF = new IntentFilter();
		iF.addAction("br.com.android.toptvz.letraencontrada");
		iF.addAction("br.com.android.toptvz.letraperdida");
		registerReceiver(mReceiver, iF);

	}
	
	@Override
	protected void onResume() {
		minhasMusicas();
		super.onResume();
	}

	private void minhasMusicas() {
		int countMusicas = contaMusicas();
		labelMinhasMusicas = (TextView) findViewById(R.id.textoBotaoMinhasLetras);
		labelMinhasMusicas.setText(String.format(
				res.getString(R.string.lbl_musicas_artistas), countMusicas,
				contaArtistas()));
		if (countMusicas == 0) {
			labelMinhasMusicasTitulo = (TextView) findViewById(R.id.textoBotaoMinhasLetrasTitulo);
			labelMinhasMusicasTitulo.setTextColor(res
					.getColor(R.color.cor_botao_sem_letra_musica));
			iconeMinhasLetras = (ImageView) findViewById(R.id.iconeBotaoMinhasLetras);
			iconeMinhasLetras.setImageDrawable(res
					.getDrawable(R.drawable.sem_minhas_musicas));
			setinhaMinhasLetras = (ImageView) findViewById(R.id.imageSetinhaMinhasLetras);
			setinhaMinhasLetras.setVisibility(View.GONE);
		} else {
			botaoMinhasLetras = (LinearLayout) findViewById(R.id.botaoMinhasLetras);
			botaoMinhasLetras.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent("br.com.android.toptvz.minhasletras");
					startActivity(i);
				}
			});
			botaoMinhasLetras.setOnTouchListener(btnTouchListener);

		}
	}
	
	
	
	private void buscar(){
		String texto = campoBusca.getText().toString();
		if(texto != null && texto.length() >= 3){
			Intent i = new Intent(getApplicationContext(), BuscaArtistasActivity.class);
			i.putExtra("strBusca", texto);
			startActivity(i);
		}
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			testaLetra();
		}
	};

	private void testaLetra() {
		temLetra = prefs.getBoolean(contexto, AppPrefs.TEM_LETRA);
		if (temLetra) {
			String nomeMusica = prefs.getString(contexto, AppPrefs.MUSICA);
			String nomeArtista = prefs.getString(contexto, AppPrefs.ARTISTA);
			Log.d("Music", "TEM LETRA");
			labelChapeu.setVisibility(View.VISIBLE);
			icone.setImageResource(R.drawable.letra_encontrada);
			labelMusica.setTextColor(res
					.getColor(R.color.cor_botao_letra_musica));
			labelMusica.setText(nomeMusica);
			labelArtista.setText(nomeArtista);
			setinhaLetra = (ImageView) findViewById(R.id.imageSetinhaLetra);
			setinhaLetra.setVisibility(View.VISIBLE);
			botaoLetra.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(
							"br.com.android.toptvz.letraencontrada");
					startActivity(i);
				}
			});
			botaoLetra.setOnTouchListener(btnTouchListener);
		} else {
			Log.d("Music", "NÃO TEM LETRA");
			botaoLetra.setOnClickListener(null);
			botaoLetra.setOnTouchListener(null);
			icone.setImageResource(R.drawable.sem_letra);
			labelMusica.setTextColor(res
					.getColor(R.color.cor_botao_sem_letra_musica));
			labelMusica.setText(R.string.lbl_letra_nao_encontrada);
			if(labelChapeu != null){
				labelChapeu.setVisibility(View.GONE);
			}
			labelArtista.setText(R.string.lbl_nao_identificou);
			setinhaLetra = (ImageView) findViewById(R.id.imageSetinhaLetra);
			if(setinhaLetra != null){
				setinhaLetra.setVisibility(View.GONE);
			}
		}
	}

	OnTouchListener btnTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int eventAction = event.getAction();
			switch (eventAction) {
			case MotionEvent.ACTION_UP: {
				v.setBackgroundDrawable(res.getDrawable(R.drawable.fundo));
				break;
			}
			case MotionEvent.ACTION_CANCEL: {
				v.setBackgroundDrawable(res.getDrawable(R.drawable.fundo));
				break;
			}
			case MotionEvent.ACTION_DOWN: {
				v.setBackgroundDrawable(res
						.getDrawable(R.drawable.fundo_pressed));
				break;
			}

			}
			return false;
		}
	};

	OnTouchListener btnTouchRankingListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int eventAction = event.getAction();
			switch (eventAction) {
			case MotionEvent.ACTION_UP: {
				v.setBackgroundDrawable(null);
				break;
			}
			case MotionEvent.ACTION_CANCEL: {
				v.setBackgroundDrawable(null);
				break;
			}
			case MotionEvent.ACTION_DOWN: {
				if(v.getId() == R.id.botaoTopArtistas){
					v.setBackgroundDrawable(res
						.getDrawable(R.drawable.fundo_top_cima));
				}else{
					v.setBackgroundDrawable(res
							.getDrawable(R.drawable.fundo_top_baixo));
				}
				break;
			}

			}
			return false;
		}
	};

	@Override
	protected void onDestroy() {
		try{
			unregisterReceiver(mReceiver);
		}catch (Exception e) {
			Util.log("erro ao tentar remover o receiver", e);
		}
		super.onDestroy();
	};
	
	private int contaMusicas() {
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();
		// executa a query pra saber se a letra existe
		String query = "select count(1) total from "
				+ TOPTVZOpenHelper.TOPTVZ_TABLE_NAME;
		// Log.d("Music", query);
		Cursor cursor = db.rawQuery(query, null);
		// verifica se o banco retornou alguma coisa
		if (cursor.getCount() > 0) {
			// posiciona no primeiro registro retornado e pega os valores
			cursor.moveToFirst();
			int total = cursor.getInt(cursor.getColumnIndex("total"));
			// fecha o cursor e o banco e retorna o objeto encontrado
			cursor.close();
			if (db != null && db.isOpen())
				db.close();

			return total;

		}
		cursor.close();
		if (db != null && db.isOpen())
			db.close();
		return 0;

	}

	private int contaArtistas() {
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();
		// executa a query pra saber se a letra existe
		String query = "select count(distinct artista) total from "
				+ TOPTVZOpenHelper.TOPTVZ_TABLE_NAME;
		// Log.d("Music", query);
		Cursor cursor = db.rawQuery(query, null);
		// verifica se o banco retornou alguma coisa
		if (cursor.getCount() > 0) {
			// posiciona no primeiro registro retornado e pega os valores
			cursor.moveToFirst();
			int total = cursor.getInt(cursor.getColumnIndex("total"));
			// fecha o cursor e o banco e retorna o objeto encontrado
			cursor.close();
			if (db != null && db.isOpen())
				db.close();

			return total;

		}
		cursor.close();
		if (db != null && db.isOpen())
			db.close();
		return 0;

	}
}
