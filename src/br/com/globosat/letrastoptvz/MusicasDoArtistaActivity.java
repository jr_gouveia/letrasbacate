package br.com.globosat.letrastoptvz;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.task.BuscaMusicasArtistaTask;
import br.com.globosat.letrastoptvz.vo.Artista;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class MusicasDoArtistaActivity extends SherlockFragmentActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		setContentView(R.layout.layout_ranking);
		ActionBar ab = getSupportActionBar();
		
		ab.setDisplayHomeAsUpEnabled(true);
		if (extras != null) {
			try {
				Artista artista = (Artista) extras.getSerializable("artista");
				ab.setTitle(artista.getNome());
				ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				if (connectivityManager.getActiveNetworkInfo() != null
						&& connectivityManager.getActiveNetworkInfo()
								.isConnected()) {
					BuscaMusicasArtistaTask task = new BuscaMusicasArtistaTask(
							this);
					task.execute(artista);
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.msg_precisa_internet, Toast.LENGTH_LONG)
							.show();
					finish();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			// put your code here
		}
		return false;
	}
}
