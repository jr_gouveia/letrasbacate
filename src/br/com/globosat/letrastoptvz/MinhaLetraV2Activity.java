package br.com.globosat.letrastoptvz;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.db.TOPTVZOpenHelper;
import br.com.globosat.letrastoptvz.sharedprefs.AppPrefs;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.TabPageIndicator;
import com.viewpagerindicator.TitleProvider;

public class MinhaLetraV2Activity extends SherlockFragmentActivity {

	LinearLayout btnOriginal;
	LinearLayout btnTraducao;
	LinearLayout btnShare;
	LinearLayout viewOriginal;
	LinearLayout viewTraducao;
	TextView textMusica;
	TextView textArtista;
	AppPrefs prefs;
	Resources res;
	LinearLayout abaAtual;
	IntentFilter iF;
	GoogleAnalyticsTracker tracker;
	ArrayList<Float> arrayTamanhoFonte = new ArrayList<Float>();
	AlertDialog dialogConfig;
	Float tamanhoTemp = Float.valueOf("16");
	Float tamanhoAtual = Float.valueOf("16");
	String alinhamentoAtual = AppPrefs.ALINHAMENTO_CENTER;
	String alinhamentoTemp = AppPrefs.ALINHAMENTO_CENTER;
	private final int MENU_ITEM_0 = 0;
	Context contexto;
	int indexAtual = -1;
	int indexTemp;
	boolean temMusica;
	View layout;
	LetraTOPTVZ letra;

	LetrasPagerAdapter mAdapter;
	ViewPager mPager;
	PageIndicator mIndicator;

	String[] abas;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", this);
		prefs = new AppPrefs();
		contexto = getApplicationContext();

		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey("letraTOP")) {
			letra = (LetraTOPTVZ) extras.get("letraTOP");
		} else {
			finish();
		}

		// cria o array de tamanhos e pega o tamanho guardado pelo usuario
		for (int i = 12; i <= 20; i++) {
			arrayTamanhoFonte.add((float) i);
			if (prefs.getFloat(contexto, AppPrefs.TAMANHO_FONTE).floatValue() == (float) i) {
				tamanhoTemp = (float) i;
				indexAtual = arrayTamanhoFonte.size() - 1;
			}
		}
		// pega o alinhamento guardado pelo usuario
		if (prefs.contains(contexto, AppPrefs.ALINHAMENTO)) {
			alinhamentoAtual = prefs.getString(contexto, AppPrefs.ALINHAMENTO);
		}

		res = getResources();

		startApplication();

	}

	private void startApplication() {
		// verifica se tem letra, senão mostra tela de "sem letra"
		if (letra != null && letra.getLetra() != null
				&& letra.getLetra().length() > 0) {
			setContentView(R.layout.layout_letrav3);
			temMusica = true;

			ActionBar ab = getSupportActionBar();
			ab.setTitle(letra.getMusica());
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
					.getDefaultDisplay();
			if ((currentapiVersion == android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH
					|| currentapiVersion == android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) && (display.getOrientation() == 1 || display.getOrientation() == 3) ) {
				Log.d("MUSIC", "ORIENTACAO ATUAL " + display.getOrientation());
			} else {
				ab.setSubtitle(letra.getArtista());
			}
			
			ab.setDisplayHomeAsUpEnabled(true);

			int theme = R.style.Theme_Sherlock_ForceOverflow;
			setTheme(theme);

			String letraTraduzida = letra.getLetraTraduzida();
			montaAbas(letraTraduzida);

			if (mAdapter == null)
				mAdapter = new LetrasPagerAdapter(getSupportFragmentManager());

			mPager = (ViewPager) findViewById(R.id.viewpager);
			// mPager.removeAllViews();

			mPager.setAdapter(mAdapter);

			mIndicator = (TabPageIndicator) findViewById(R.id.indicator);
			mIndicator.setViewPager(mPager);

			// mAdapter.notifyDataSetChanged();
			startConfig();

			tracker.trackPageView("/minha-letra");
		} else {
			Toast.makeText(this, "Não foi possível abrir a letra da música",
					Toast.LENGTH_SHORT).show();
			finish();
		}

		tracker.dispatch();

	}

	private void montaAbas(String letraTraduzida) {
		if (letraTraduzida != null && letraTraduzida.trim().length() > 0) {
			abas = new String[2];
			abas[0] = "original";
			abas[1] = "tradução";
		} else {
			abas = new String[1];
			abas[0] = "original";
			LinearLayout linearAbas = (LinearLayout) findViewById(R.id.LinearLayoutAbas);
			if (linearAbas != null) {
				linearAbas.setVisibility(View.GONE);
			}
		}
	}

	private void carregaConfig() {
		// cria o array de tamanhos e pega o tamanho guardado pelo usuario
		for (int i = 12; i <= 20; i++) {
			arrayTamanhoFonte.add((float) i);
			if (prefs.getFloat(contexto, AppPrefs.TAMANHO_FONTE).floatValue() == (float) i) {
				tamanhoTemp = (float) i;
				tamanhoAtual = (float) i;
				indexAtual = arrayTamanhoFonte.size() - 1;
			}
		}
		// pega o alinhamento guardado pelo usuario
		if (prefs.contains(contexto, AppPrefs.ALINHAMENTO)) {
			alinhamentoAtual = prefs.getString(contexto, AppPrefs.ALINHAMENTO);
		}

		tamanhoTemp = tamanhoAtual;
		alinhamentoTemp = alinhamentoAtual;
	}

	private void startConfig() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layout = inflater.inflate(R.layout.config_dialog,
				(ViewGroup) findViewById(R.id.config_root));
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
				.setView(layout);
		builder.setIcon(R.drawable.ic_action_config);
		builder.setMessage("Configurações                ")
				.setCancelable(false)
				.setPositiveButton("Salvar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Log.d("Music", "CLICOU NO BOTAO SALVAR");
								prefs.putFloat(contexto,
										AppPrefs.TAMANHO_FONTE, tamanhoTemp);
								prefs.putString(contexto, AppPrefs.ALINHAMENTO,
										alinhamentoTemp);
								indexAtual = indexTemp;
								alinhamentoAtual = alinhamentoTemp;
								tamanhoAtual = tamanhoTemp;
							}
						})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Log.d("Music", "CLICOU NO BOTAO CANCELAR");
								try {
									if (mAdapter != null) {
										mAdapter.updateConfigs(
												prefs.getFloat(contexto,
														AppPrefs.TAMANHO_FONTE)
														.floatValue(),
												prefs.getString(contexto,
														AppPrefs.ALINHAMENTO));
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
								atualizaSeekFonte(layout, indexAtual);
								atualizaRadioAlinhamento(layout,
										alinhamentoAtual);

								dialog.cancel();
							}
						});

		dialogConfig = builder.create();

		atualizaSeekFonte(layout, indexAtual);

		atualizaRadioAlinhamento(layout, alinhamentoAtual);

		// alertDialog.show();
	}

	private void atualizaSeekFonte(View layout, int index) {
		SeekBar seekBar = (SeekBar) layout.findViewById(R.id.seekBarFonte);
		seekBar.setOnSeekBarChangeListener(seekListener);
		// setando a posição inicial do seekbar
		if (index >= 0) {
			seekBar.setProgress(index);
		}
	}

	private void atualizaRadioAlinhamento(View layout, String alinhamento) {
		try {
			RadioGroup rg = (RadioGroup) layout
					.findViewById(R.id.GroupAlinhamento);
			rg.setOnCheckedChangeListener(radioListener);

			// inicializando o radiobutton selecionado
			RadioButton rbLeft = (RadioButton) layout
					.findViewById(R.id.ButtonLeft);
			RadioButton rbCenter = (RadioButton) layout
					.findViewById(R.id.ButtonCenter);
			if (alinhamento.equalsIgnoreCase(AppPrefs.ALINHAMENTO_LEFT)) {
				rbLeft.setChecked(true);
			} else {
				rbCenter.setChecked(true);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// a cada mudança na seekbar ele atualiza em tempo real a letra que está ao
	// fundo.
	OnSeekBarChangeListener seekListener = new SeekBar.OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// seekBarValue.setText(arrayIntervalo.get(Integer.valueOf(progress))+" "+res.getString(R.string.minutos));
			try {
				// TextView textLetra = (TextView)
				// findViewById(R.id.TextViewLetraOriginal);
				// TextView textLetraTraduzida = (TextView)
				// findViewById(R.id.TextViewLetraTraduzida);
				// textLetra.setTextSize(arrayTamanhoFonte.get(Integer
				// .valueOf(progress)));
				// textLetraTraduzida.setTextSize(arrayTamanhoFonte.get(Integer
				// .valueOf(progress)));
				tamanhoTemp = arrayTamanhoFonte.get(Integer.valueOf(progress));
				if (mAdapter != null) {
					mAdapter.updateConfigs(tamanhoTemp, alinhamentoTemp);
				}
				indexTemp = progress;
			} catch (Exception e) {
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
		}
	};

	// a cada mudança no alinhamento ele atualiza em tempo real a letra que está
	// ao fundo.
	OnCheckedChangeListener radioListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// RadioButton rb = (RadioButton) group.findViewById(checkedId);
			if (checkedId == R.id.ButtonCenter) {
				Log.d("teste", "radio selecionado: center");
				alinhamentoTemp = AppPrefs.ALINHAMENTO_CENTER;
			} else {
				Log.d("teste", "radio selecionado: left");
				alinhamentoTemp = AppPrefs.ALINHAMENTO_LEFT;
			}
			if (mAdapter != null) {
				mAdapter.updateConfigs(tamanhoTemp, alinhamentoTemp);
			}
		}
	};

	class LetrasPagerAdapter extends FragmentPagerAdapter implements
			TitleProvider {
		public LetraFragment[] fragmentosCarregados;

		public LetrasPagerAdapter(FragmentManager fm) {
			super(fm);
			fragmentosCarregados = new LetraFragment[abas.length];
		}

		@Override
		public Fragment getItem(int position) {
			Bundle bundle = new Bundle();
			switch (position) {
			case 0:
				bundle.clear();
				bundle.putString("titulo", "original");
				bundle.putString("letra", letra.getLetra());
				bundle.putFloat("tamanho", tamanhoAtual);
				bundle.putString("alinhamento", alinhamentoAtual);
				LetraFragment frag = LetraFragment.newInstance("original");
				frag.setArguments(bundle);
				fragmentosCarregados[position] = frag;
				return frag;

			case 1:
				bundle.clear();
				bundle.putString("titulo", "tradução");
				bundle.putString("letra", letra.getLetraTraduzida());
				bundle.putFloat("tamanho", tamanhoAtual);
				bundle.putString("alinhamento", alinhamentoAtual);
				LetraFragment frag1 = LetraFragment.newInstance("tradução");

				frag1.setArguments(bundle);
				fragmentosCarregados[position] = frag1;
				return frag1;
			default:
				return null;

			}
		}

		@Override
		public int getCount() {
			return abas.length;
		}

		@Override
		public String getTitle(int position) {
			return abas[position % abas.length].toLowerCase();
		}

		public void updateViews() {
			for (LetraFragment frag : fragmentosCarregados) {
				Log.d("Music", "loop de fragmentos carregados");
				if (frag != null) {
					Log.d("Music", "não é nulo");
					if (frag.titulo.equalsIgnoreCase("original")) {
						Log.d("Music", "original");
						frag.update(prefs.getString(contexto, AppPrefs.LETRA));
					} else if (frag.titulo.equalsIgnoreCase("tradução")) {
						Log.d("Music", "tradução");
						frag.update(prefs.getString(contexto,
								AppPrefs.LETRA_TRADUZIDA));
					}
				} else {
					Log.d("Music", "é nulo");
				}

			}
		}

		public void updateConfigs(float tamanho, String alinhamento) {
			for (LetraFragment frag : fragmentosCarregados) {
				Log.d("Music", "loop de fragmentos carregados config");
				if (frag != null) {
					Log.d("Music", "não é nulo");
					frag.updateConfig(tamanho, alinhamento);
				} else {
					Log.d("Music", "é nulo");
				}

			}
		}

		@Override
		public void destroyItem(View pager, int position, Object view) {
		}

	}

	private boolean salvaMusicaDB(LetraTOPTVZ letra) {
		// pega a instancia do banco
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();

		// seta os valores a serem gravados
		ContentValues values = new ContentValues();
		values.put("_id_musica", letra.getIdMusica());
		values.put("id_artista", letra.getIdArtista());
		values.put("artista", letra.getArtista());
		values.put("musica", letra.getMusica());
		values.put("letra", letra.getLetra());
		values.put("letra_traduzida", letra.getLetraTraduzida());
		Log.d("Music", "id artista = " + letra.getIdArtista()
				+ "  -  id musica = " + letra.getIdMusica());

		// tenta inserir, se o retorno for -1 é pq algum erro aconteceu
		long id = db.insert(TOPTVZOpenHelper.TOPTVZ_TABLE_NAME, null, values);
		if (id >= 0) {
			Log.d("Music", "gravou letra encontrada no banco de dados");
		} else {
			Log.d("Music",
					"nao conseguiu gravar letra encontrada no banco de dados");
		}
		if (db != null && db.isOpen())
			db.close();
		return true;
	}

	private boolean consultaMusicaDB(int idMusica, int idArtista) {
		// pega uma instancia do banco
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getReadableDatabase();
		if (idArtista != 0 && idMusica != 0) {
			// executa a query pra saber se a letra existe
			String query = "select * from "
					+ TOPTVZOpenHelper.TOPTVZ_TABLE_NAME
					+ " where id_artista = " + idArtista + " and _id_musica = "
					+ idMusica + " limit 1";
			Cursor cursor = db.rawQuery(query, null);
			Log.d("Music", "query: " + query);
			Log.d("Music", "qtd: " + cursor.getCount());
			if (cursor.getCount() > 0) {
				cursor.close();
				if (db != null && db.isOpen())
					db.close();
				return true;
			}
			cursor.close();
			if (db != null && db.isOpen())
				db.close();
		}
		return false;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(1, 0, 1, "Compartilhar").setIcon(R.drawable.ic_action_share)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add(1, 1, 2, "Configurações").setIcon(R.drawable.ic_action_config)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		Log.d("Music", "consulta: " + consultaMusicaDB(letra.getIdMusica(), letra.getIdArtista()));
		if (! consultaMusicaDB(letra.getIdMusica(), letra.getIdArtista())) {
			menu.add(0, 2, 0, "Salvar").setIcon(R.drawable.ic_action_star)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// Intent intentUp = new Intent(this, InicialActivity.class);
			// intentUp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// startActivity(intentUp);

			finish();
			return true;
		case 0:
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					res.getString(R.string.app_name));
			intent.putExtra(Intent.EXTRA_TEXT, String.format(
					res.getString(R.string.compartilharTexto),
					letra.getArtista() + " - " + letra.getMusica()));

			startActivity(Intent.createChooser(intent, "Compartilhar"));
			tracker.trackEvent("Letra", // Category
					"Click", // Action
					"Compartilhar", // Label
					3); // Value
			tracker.dispatch();
			break;
		case 1:
			dialogConfig.show();
			break;
		case 2:
			salvaMusicaDB(letra);
			Toast.makeText(this, "Esta letra foi salva em \"Minhas letras\" com sucesso!", Toast.LENGTH_LONG)
					.show();
			item.setVisible(false);
			break;
		default:
			// put your code here
		}
		return false;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
//		if (temMusica) {
//			menu.getItem(1).setVisible(true);
//		} else {
//			menu.getItem(1).setVisible(false);
//		}
//		try {
//			// mostra estrela se a musica ainda nao estiver salva
//			Log.d("Music", "consulta2: " + consultaMusicaDB(letra.getIdMusica(), letra.getIdArtista()));
//			if (consultaMusicaDB(letra.getIdMusica(), letra.getIdArtista())) {
//				menu.getItem(0).setVisible(false);
//			} else {
//				menu.getItem(0).setVisible(true);
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		return super.onPrepareOptionsMenu(menu);
	}
}
