package br.com.globosat.letrastoptvz;

import java.util.Set;

import br.com.globosat.letrastoptvz.util.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MusicBroadcastReceiver extends BroadcastReceiver {
	
	
	@Override
    public void onReceive(Context context, Intent intent) {
    	
		//Log.d("Music", "Abriu  o Broadcaster");
		//Recebe o broadcast enviado pelo player e prepara a intent pra passar pro service.
		if(intent != null){
			Intent intentMusic = new Intent();
			intentMusic.setAction("br.com.globosat.letrastoptvz.MusicService");

			try{
				intentMusic.putExtras(intent.getExtras());
				intentMusic.putExtra("actionOriginal", intent.getAction());
				context.startService(intentMusic);
			}catch (Exception e) {
				Log.e("Music", "Erro ao receber os extras", e);
				
			}
		}
    }
	

}