package br.com.globosat.letrastoptvz;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.task.BuscaLetraTask;
import br.com.globosat.letrastoptvz.vo.Musica;

public class RepassaLetra extends Activity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_ranking);
		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey("musicaTOP")) {
			Musica m = (Musica) extras.get("musicaTOP");
			ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivityManager.getActiveNetworkInfo() != null
					&& connectivityManager.getActiveNetworkInfo().isConnected()) {
				BuscaLetraTask task = new BuscaLetraTask(this);
				task.execute(m);
			} else {
				Toast.makeText(getApplicationContext(),
						R.string.msg_precisa_internet, Toast.LENGTH_LONG)
						.show();
				finish();
			}
		} else {
			finish();
		}
	}
}
