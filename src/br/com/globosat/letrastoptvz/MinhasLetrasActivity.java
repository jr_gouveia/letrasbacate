package br.com.globosat.letrastoptvz;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import br.com.globosat.letrastoptvz.adapter.MusicaAdapter;
import br.com.globosat.letrastoptvz.db.TOPTVZOpenHelper;
import br.com.globosat.letrastoptvz.task.BuscaLetrasDBThread;
import br.com.globosat.letrastoptvz.vo.LetraTOPTVZ;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MinhasLetrasActivity extends SherlockFragmentActivity {

	private GestureDetector mGestureDetector;

	// x and y coordinates within our side index
	private static float sideIndexX;
	private static float sideIndexY;
	GoogleAnalyticsTracker tracker;
	// height of side index
	private int sideIndexHeight;

	// number of items in the side index
	private int indexListSize;
	private ArrayList<LetraTOPTVZ> lista = new ArrayList<LetraTOPTVZ>();
	private ArrayList<LetraTOPTVZ> listaFinal = new ArrayList<LetraTOPTVZ>();

	// list with items for side index
	private ArrayList<Object[]> indexList = null;
	private BuscaLetrasDBThread processo;
	private ProgressBar loading = null;

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// progressDialog.dismiss();
			switch (msg.what) {
			case 0:
				if (processo != null) {
					lista = processo.lista;
					escreveLista(lista);
				}
				// Util.pesquisarTime(CartolaActivity.this, listaTimes);
				break;
			case 3:
				// erroEncontrado();
				break;
			case 4:
				// if(processo != null){
				// String texto = processo.textoAlerta;
				// if(texto.length() > 0){
				// textoAlerta.setText(texto);
				// LinearLayout boxAlerta = (LinearLayout)
				// findViewById(R.id.BoxAlerta);
				// boxAlerta.setVisibility(View.VISIBLE);
				// }
				// }
				break;
			default:
				break;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_minhas_letras);

		ActionBar ab = getSupportActionBar();
		ab.setTitle("minhas letras");
		ab.setDisplayHomeAsUpEnabled(true);

		// restartActivity(savedInstanceState);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession("UA-21880502-1", this);
		loading = (ProgressBar) findViewById(R.id.loading);
		loading.setIndeterminate(true);
		if (getLastNonConfigurationInstance() != null) {
			processo = (BuscaLetrasDBThread) getLastNonConfigurationInstance();
			processo.HandlerOfCaller = handler;
			// Util.log("status do processo " + processo.GetStatus());
			switch (processo.GetStatus()) {
			case BuscaLetrasDBThread.STATE_RUNNING:
				Log.d("sei la", "state running");
				// Util.mostraLoading(CartolaActivity.this, progressDialog,
				// processo.titulo, processo.mensagem);

				break;
			case BuscaLetrasDBThread.STATE_NOT_STARTED:
				Log.d("sei la", "state not started");
				// Util.escondeLoading(progressDialog);
				// restart(savedInstanceState);
				restartActivity(savedInstanceState);
				break;
			case BuscaLetrasDBThread.STATE_DONE:
				Log.d("sei la", "state done");
				processo = null;
				restartActivity(savedInstanceState);
				// Util.escondeLoading(progressDialog);
				// restart(savedInstanceState);
				break;
			case BuscaLetrasDBThread.STATE_ERROR:
				Log.d("sei la", "state error");
				processo = null;
				// Util.escondeLoading(progressDialog);
				// carregaTelaPrincipal();
				break;
			default:
				// Util.escondeLoading(progressDialog);
				Log.d("sei la", "state default");
				processo.interrupt();
				processo = null;
				restartActivity(savedInstanceState);
				// restart(savedInstanceState);
				break;
			}
		} else {

			processo = new BuscaLetrasDBThread(handler, getApplicationContext());
			processo.start();

		}
	}

	private void restartActivity(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			Object objLista = savedInstanceState.getSerializable("lista");
			if (objLista != null) {
				lista = (ArrayList<LetraTOPTVZ>) objLista;
				escreveLista(lista);

			} else {
				processo = new BuscaLetrasDBThread(handler,
						getApplicationContext());
				processo.start();
			}

		} else {
			processo = new BuscaLetrasDBThread(handler, getApplicationContext());
			processo.start();
		}
	}

	private void escreveLista(ArrayList<LetraTOPTVZ> listaMusicas) {
		listaFinal = listaMusicas;

		tracker.trackPageView("/letra");
		MusicaComparator c = new MusicaComparator();
		java.util.Collections.sort(listaFinal, c);

		final ListView lv1 = (ListView) findViewById(R.id.ListMinhasMusicas);
		lv1.setAdapter(new MusicaAdapter(this, R.layout.musica_item, listaFinal));

		lv1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent i = new Intent("br.com.android.toptvz.minhaletrav2");
				i.putExtra("letraTOP", listaFinal.get(position));
				startActivity(i);
			}
		});

		lv1.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				try {
					final int posicao = position;
					AlertDialog.Builder builder = new AlertDialog.Builder(
							MinhasLetrasActivity.this);
					builder.setMessage("Deseja apagar esta música?")
							.setCancelable(false)
							.setPositiveButton("Sim",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											boolean deletou = deletaMusicaDB(listaFinal
													.get(posicao));
											if (deletou) {
												listaFinal.remove(posicao);
												escreveLista(listaFinal);
											}
										}
									})
							.setNegativeButton("Não",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});
					AlertDialog alertDialog = builder.create();
					alertDialog.show();

				} catch (Exception e) {
					Log.e("Music",
							"nao conseguiu deletar letra do banco de dados", e);
				}
				return false;
			}
		});

		mGestureDetector = new GestureDetector(this,
				new SideIndexGestureListener());

		tracker.dispatch();
		if (listaFinal.size() > 0) {
			final ListView listView = (ListView) findViewById(R.id.ListMinhasMusicas);
			LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
			sideIndexHeight = sideIndex.getHeight();
			sideIndex.removeAllViews();

			// TextView for every visible item
			TextView tmpTV = null;

			// we'll create the index list
			indexList = createIndex(listaFinal);

			// number of items in the index List
			indexListSize = indexList.size();
			if(indexListSize >= 10){
			// maximal number of item, which could be displayed
				int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 17);
	
				int tmpIndexListSize = indexListSize;
	
				// handling that case when indexListSize > indexMaxSize
				while (tmpIndexListSize > indexMaxSize) {
					tmpIndexListSize = tmpIndexListSize / 2;
				}
	
				// computing delta (only a part of items will be displayed to
				// save a
				// place)
				double delta = indexListSize / tmpIndexListSize;
	
				String tmpLetter = null;
				Object[] tmpIndexItem = null;
	
				// show every m-th letter
				for (double i = 1; i <= indexListSize; i = i + delta) {
					tmpIndexItem = indexList.get((int) i - 1);
					tmpLetter = tmpIndexItem[0].toString();
					tmpTV = new TextView(this);
					tmpTV.setText(tmpLetter);
					tmpTV.setGravity(Gravity.CLIP_HORIZONTAL);
					tmpTV.setTextSize(12);
					// tmpTV.setTextColor(color.side_index_text);
					LayoutParams params = new LayoutParams(
							LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
					tmpTV.setLayoutParams(params);
					sideIndex.addView(tmpTV);
				}
	
				// and set a touch listener for it
				sideIndex.setOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// now you know coordinates of touch
						sideIndexX = event.getX();
						sideIndexY = event.getY();
	
						// and can display a proper item it country list
						displayListItem();
	
						return false;
					}
				});
			}else{
				sideIndex.setVisibility(View.GONE);
			}
			loading.setVisibility(View.GONE);
		} else {
			Log.d("Music", "lista vazia");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("lista", lista);
		super.onSaveInstanceState(outState);
		// Util.log("******************SALVOU A INSTANCIA");
	}

	@Override
	protected void onRestoreInstanceState(Bundle outState) {
		super.onRestoreInstanceState(outState);
	}

	// @Override
	// public Object onRetainNonConfigurationInstance() {
	// if (processo != null) {
	// processo.HandlerOfCaller = null;
	// return (processo);
	// }
	// return super.onRetainNonConfigurationInstance();
	// }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mGestureDetector.onTouchEvent(event)) {
			return true;
		} else {
			return false;
		}
	}

	public String converte(String text) {

		return text.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e")
				.replaceAll("[îìíï]", "i").replaceAll("[õôòóö]", "o")
				.replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A")
				.replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I")
				.replaceAll("[ÕÔÒÓÖ]", "O").replaceAll("[ÛÙÚÜ]", "U")
				.replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n')
				.replace('Ñ', 'N').replaceAll("!", "")
				.replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ")
				.replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ")
				.replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/]", " ");

	}

	private ArrayList<Object[]> createIndex(ArrayList<LetraTOPTVZ> listaLetras) {
		ArrayList<Object[]> tmpIndexList = new ArrayList<Object[]>();
		Object[] tmpIndexItem = null;

		int tmpPos = 0;
		String tmpLetter = "";
		String currentLetter = null;
		String strItem = null;

		for (int j = 0; j < listaLetras.size(); j++) {
			strItem = converte(listaLetras.get(j).getMusica());
			currentLetter = strItem.substring(0, 1);
			try {
				int numero = Integer.parseInt(currentLetter);
				currentLetter = "#";
			} catch (Exception e) {
				// TODO: handle exception
			}

			// every time new letters comes
			// save it to index list
			if (!currentLetter.equalsIgnoreCase(tmpLetter)) {
				tmpIndexItem = new Object[3];
				tmpIndexItem[0] = tmpLetter;
				tmpIndexItem[1] = tmpPos - 1;
				tmpIndexItem[2] = j - 1;

				tmpLetter = currentLetter.toLowerCase();
				tmpPos = j + 1;

				tmpIndexList.add(tmpIndexItem);
				// Log.d("Music", "LETRA: " + tmpLetter);
			}
		}

		// save also last letter
		tmpIndexItem = new Object[3];
		tmpIndexItem[0] = tmpLetter.toLowerCase();
		tmpIndexItem[1] = tmpPos - 1;
		tmpIndexItem[2] = listaLetras.size() - 1;
		tmpIndexList.add(tmpIndexItem);

		// and remove first temporary empty entry
		if (tmpIndexList != null && tmpIndexList.size() > 0) {
			tmpIndexList.remove(0);
		}

		return tmpIndexList;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

	}

	class SideIndexGestureListener extends
			GestureDetector.SimpleOnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			// we know already coordinates of first touch
			// we know as well a scroll distance
			sideIndexX = sideIndexX - distanceX;
			sideIndexY = sideIndexY - distanceY;

			// when the user scrolls within our side index
			// we can show for every position in it a proper
			// item in the country list
			if (sideIndexX >= 0 && sideIndexY >= 0) {
				displayListItem();
			}

			return super.onScroll(e1, e2, distanceX, distanceY);
		}
	}

	public void displayListItem() {
		// compute number of pixels for every side index item
		double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

		// compute the item index for given event position belongs to
		int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

		// compute minimal position for the item in the list
		int minPosition = (int) (itemPosition * pixelPerIndexItem);

		// get the item (we can do it since we know item index)
		Object[] indexItem = indexList.get(itemPosition);

		// and compute the proper item in the country list
		int indexMin = Integer.parseInt(indexItem[1].toString());
		int indexMax = Integer.parseInt(indexItem[2].toString());
		int indexDelta = Math.max(1, indexMax - indexMin);

		double pixelPerSubitem = pixelPerIndexItem / indexDelta;
		int subitemPosition = (int) (indexMin + (sideIndexY - minPosition)
				/ pixelPerSubitem);

		ListView listView = (ListView) findViewById(R.id.ListMinhasMusicas);
		listView.setSelection(subitemPosition);
	}

	private ArrayList<LetraTOPTVZ> consultaMusicasDB() {
		// pega uma instancia do banco
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();
		ArrayList<LetraTOPTVZ> listaLetras = new ArrayList<LetraTOPTVZ>();
		// executa a query pra saber se a letra existe
		String query = "select * from " + TOPTVZOpenHelper.TOPTVZ_TABLE_NAME;
		// + " order by musica asc";
		// Log.d("Music", query);
		Cursor cursor = db.rawQuery(query, null);
		// verifica se o banco retornou alguma coisa
		if (cursor.getCount() > 0) {
			// posiciona no primeiro registro retornado e pega os valores
			cursor.moveToFirst();
			do {
				int idMusicaDB = cursor.getInt(cursor
						.getColumnIndex("_id_musica"));
				int idArtistaDB = cursor.getInt(cursor
						.getColumnIndex("id_artista"));
				String artistaDB = cursor.getString(cursor
						.getColumnIndex("artista"));
				String musicaDB = cursor.getString(cursor
						.getColumnIndex("musica"));
				String letraDB = cursor.getString(cursor
						.getColumnIndex("letra"));
				String letraTraduzidaDB = cursor.getString(cursor
						.getColumnIndex("letra_traduzida"));

				LetraTOPTVZ letraEncontrada = new LetraTOPTVZ(idMusicaDB,
						idArtistaDB, artistaDB, musicaDB, letraDB,
						letraTraduzidaDB, "", "");

				listaLetras.add(letraEncontrada);

				cursor.moveToNext();
			} while (!cursor.isAfterLast());

		}
		cursor.close();
		if (db != null && db.isOpen())
			db.close();
		return listaLetras;

	}

	private boolean deletaMusicaDB(LetraTOPTVZ letra) {
		// pega a instancia do banco
		SQLiteDatabase db = null;
		TOPTVZOpenHelper toh = new TOPTVZOpenHelper(getApplicationContext());
		db = toh.getWritableDatabase();

		// seta os valores a serem gravados
		ContentValues values = new ContentValues();
		values.put("_id_musica", letra.getIdMusica());
		values.put("id_artista", letra.getIdArtista());
		values.put("artista", letra.getArtista());
		values.put("musica", letra.getMusica());
		values.put("letra", letra.getLetra());
		values.put("letra_traduzida", letra.getLetraTraduzida());

		// tenta inserir, se o retorno for -1 é pq algum erro aconteceu
		long qtd = db.delete(TOPTVZOpenHelper.TOPTVZ_TABLE_NAME,
				"_id_musica = " + letra.getIdMusica(), null);
		if (db != null && db.isOpen())
			db.close();
		if (qtd > 0) {
			Log.d("Music", "deletou letra do banco de dados");
			return true;
		} else {
			Log.d("Music", "nao conseguiu deletar letra do banco de dados");
			return false;
		}

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			// put your code here
		}
		return false;
	}
}
