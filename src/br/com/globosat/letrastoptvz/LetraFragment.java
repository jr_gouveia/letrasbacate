package br.com.globosat.letrastoptvz;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import br.com.globosat.letrastoptvz.sharedprefs.AppPrefs;

public final class LetraFragment extends Fragment {

	String letra;
	public String titulo = "";
	String alinhamento;
	float tamanho;

	public static LetraFragment newInstance(String titulo) {
		LetraFragment fragment = new LetraFragment();

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		letra = getArguments().getString("letra");
		titulo = getArguments().getString("titulo");
		alinhamento = getArguments().getString("alinhamento");
		tamanho = getArguments().getFloat("tamanho");

		View view;
		view = inflater.inflate(R.layout.letra_fragment, container, false);
		updateLetra(letra, view);
		updateConfig(view);
		return view;
	}

	// textLetra.setTextSize(prefs.getFloat(contexto,
	// AppPrefs.TAMANHO_FONTE));
	private void updateLetra(String letra, View view) {
		TextView texto = (TextView) view.findViewById(R.id.TextViewLetra);
		if (texto != null) {
			texto.setText(Html.fromHtml(letra));
		}
	}

	private void updateConfig(View view) {
		TextView texto = (TextView) view.findViewById(R.id.TextViewLetra);
		try {
			if (texto != null) {
				texto.setTextSize(tamanho);
			}

			if (alinhamento.equalsIgnoreCase(AppPrefs.ALINHAMENTO_CENTER)) {
				texto.setGravity(Gravity.CENTER_HORIZONTAL);
			} else {
				texto.setGravity(Gravity.LEFT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void update(String letraNova) {
		this.letra = letraNova;
		updateLetra(letraNova, getView());
	}

	public void updateConfig(float tamanho, String alinhamento) {
		this.tamanho = tamanho;
		this.alinhamento = alinhamento;
		updateConfig(getView());
	}

}
