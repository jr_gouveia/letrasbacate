package br.com.globosat.letrastoptvz;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import br.com.globosat.letrastoptvz.adapter.RankingArtistaAdapter;
import br.com.globosat.letrastoptvz.task.BuscaArtistasTask;
import br.com.globosat.letrastoptvz.util.Util;
import br.com.globosat.letrastoptvz.vo.Artista;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class BuscaArtistasActivity extends SherlockFragmentActivity {

	String strBusca = "";
	ArrayList<Artista> listaArtistas = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_ranking);

		if (savedInstanceState != null) {
			recuperaDados(savedInstanceState);
		} else {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				if (extras.containsKey("strBusca")) {
					strBusca = extras.getString("strBusca");
				} else {
					Toast.makeText(this, "Não foi possível realizar a busca",
							Toast.LENGTH_SHORT).show();
					finish();
				}
				if (extras.containsKey("listaArtistas")) {
					listaArtistas = (ArrayList<Artista>) extras
							.get("listaArtistas");
				}
			}
		}

		ActionBar ab = getSupportActionBar();
		ab.setTitle("artistas com \"" + strBusca + "\"");
		ab.setDisplayHomeAsUpEnabled(true);

		criaReceiver();

		carregaDados();

	}

	private void carregaDados() {
		if (listaArtistas != null) {
			atualizaViews(listaArtistas);
			ProgressBar loading = (ProgressBar) findViewById(R.id.loading);
			loading.setVisibility(View.GONE);
		} else {
			BuscaArtistasTask task = new BuscaArtistasTask(this, strBusca);
			task.execute();
		}

	}

	private void atualizaViews(ArrayList<Artista> listaArtistas) {
		final ListView lv1 = (ListView) findViewById(R.id.ListRanking);
		lv1.setAdapter(new RankingArtistaAdapter(this,
				R.layout.ranking_artista_item, listaArtistas, false));
		lv1.setOnItemClickListener(listenerArtista);

	}

	OnItemClickListener listenerArtista = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			
			Artista artista = listaArtistas.get(position);
			selecionaArtista(artista);
		}
	};
	
	private void selecionaArtista(Artista artista) {
		Intent intent = new Intent("br.com.android.toptvz.musicasdoartista");
		intent.putExtra("artista", artista);
		startActivity(intent);
	}
	
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.hasExtra("listaArtistas")) {
				listaArtistas = (ArrayList<Artista>) intent
						.getSerializableExtra("listaArtistas");
				if(listaArtistas!= null && listaArtistas.size() > 0){
					TextView texto = (TextView) findViewById(R.id.textNenhumResultado);
					texto.setVisibility(View.GONE);
					ListView lv1 = (ListView) findViewById(R.id.ListRanking);
					lv1.setVisibility(View.VISIBLE);
					if(listaArtistas.size() == 1){
						Artista artista = listaArtistas.get(0);
						selecionaArtista(artista);
						finish();
					}else{
						atualizaViews(listaArtistas);
					}
				}else{
					TextView texto = (TextView) findViewById(R.id.textNenhumResultado);
					texto.setVisibility(View.VISIBLE);
					ListView lv1 = (ListView) findViewById(R.id.ListRanking);
					lv1.setVisibility(View.GONE);
				}
			}

			ProgressBar loading = (ProgressBar) findViewById(R.id.loading);
			loading.setVisibility(View.GONE);

			if (intent.hasExtra("erroConexao")
					&& intent.getBooleanExtra("erroConexao", false)) {
				Util.toastErroConexao(BuscaArtistasActivity.this);
			}
		}
	};

	public void criaReceiver() {
		IntentFilter iF = new IntentFilter();
		iF.addAction(BuscaArtistasTask.INTENT);
		registerReceiver(mReceiver, iF);
	}

	public void destroiReceiver() {
		try {
			unregisterReceiver(mReceiver);
		} catch (Exception e) {
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Util.log("ON SAVE");
		if (listaArtistas != null) {
			outState.putSerializable("listaArtistas", listaArtistas);
		}
		outState.putString("strBusca", strBusca);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Util.log("ON RESTORE");
		recuperaDados(savedInstanceState);
		super.onRestoreInstanceState(savedInstanceState);
	}

	private void recuperaDados(Bundle savedInstanceState) {
		if (savedInstanceState.containsKey("listaArtistas")) {
			listaArtistas = (ArrayList<Artista>) savedInstanceState
					.get("listaArtistas");
		}
		if (savedInstanceState.containsKey("strBusca")) {
			strBusca = savedInstanceState.getString("strBusca");
		}
	}

	@Override
	protected void onDestroy() {
		destroiReceiver();
		super.onDestroy();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			// put your code here
		}
		return false;
	}
}
